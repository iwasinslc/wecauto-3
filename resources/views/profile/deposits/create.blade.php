@extends('layouts.profile')
@section('title', __('Car order'))
@section('content')
    <section class="order-step">
        <div class="container">
            <div class="car-orders">
                <h3 class="lk-title">{{__('Car order')}}
                </h3>
            </div>
            <ul class="steps-indicator">
                <li class="is-active is-current"> <span>1</span>
                </li>
                <li> <span>2</span>
                </li>
                <li> <span>3</span>
                </li>
                <li> <span>4</span>
                </li>
            </ul>
            <div class="steps-title">
                <h4><span class="color-warning">{{__('Step')}}</span> #1</h4><small>{{__('Enter the Make and model of the car')}}</small>
            </div>
            <div class="steps-intro">
                <p>{{__('Select Car/Moto')}}</p>
                <p class="color-primary"> <strong>{{__('Specify the vehicle brand, model, year of issue')}}</strong></p>
            </div>
        </div>
    </section>
    <section class="select-vehicle">
        <div class="container">
            <div class="tabs js-tabs">
                <div data-title="{{__('Car')}}">
                    <form method="post" action="{{route('profile.deposits.create_handle')}}" style="width: 100%">
                                {{csrf_field()}}
                    <div class="calculate-tab">
                        <div class="calc-form">

                                <div class="field">

                                    <input type="text" required name="name" placeholder="{{__('Car model')}}" value="{{isset($data['name']) ? $data['name'] : ''}}">
                                    <input type="hidden" name="type" value="0">

                                </div>





                        </div>
                    </div>
                    <div class="order-step__buttons">
                        <button type="submit" id="next_step" class="btn btn--warning btn--size-md">{{__('Next step')}}</button>
                    </div>
                     </form>
                </div>
                <div data-title="{{__('Moto')}}">
                    <form method="post" action="{{route('profile.deposits.create_handle')}}" style="width: 100%">
                                {{csrf_field()}}
                    <div class="calculate-tab">
                        <div class="calc-form">

                                <div class="field">

                                    <input type="text" name="name" required placeholder="{{__('Moto model')}}" value="{{isset($data['name']) ? $data['name'] : ''}}">
                                    <input type="hidden" name="type" value="1">
                                </div>


                        </div>
                    </div>
                        <div class="order-step__buttons">
                            <button type="submit" id="next_step" class="btn btn--warning btn--size-md">{{__('Next step')}}</button>
                        </div>
                       </form>
                </div>

            </div>

            <div class="select-vehicle__row">
                <div class="select-vehicle__col">
{{--                    <div class="typography">--}}
{{--                        <p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</strong></p>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmodLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt  ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>--}}
{{--                    </div>--}}
                </div>
                <div class="select-vehicle__col">
                    <div class="select-vehicle__image"><img src="/assets/images/cars.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.card -->
@endsection

@push('load-scripts')
{{--    <script>--}}
{{--        $('body').on('click', '#next_step', function (e) {--}}
{{--            $('.tabs__content.is__active').find('form').submit();--}}
{{--        });--}}
{{--    </script>--}}
@endpush