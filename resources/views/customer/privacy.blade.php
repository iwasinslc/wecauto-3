@extends('layouts.auth')
@section('title', __('Privacy Policy'))
@section('content')
    <section class="page-preview">
        <div class="container">
                                <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('Privacy Policy')}}</span>
            </div>
            <h1 class="page-preview__title">Пользовательское <span class="color-primary">соглашение</span>
            </h1>
        </div>
    </section>
    <section class="terms">
        <div class="container">
            <div class="typography">
                <p class="typography-intro-text"> <strong>
                        <big>Политика конфиденциальности является документом, регламентирующим неразглашение личных данных пользователей со стороны компании и обеспечение конфиденциальности в процессе сотрудничества с клиентами.</big></strong></p>
                <p> <strong>1. Защита конфиденциальных данных</strong></p>
                <p>1.1. WECAUTO гарантирует конфиденциальность информации, получаемой от пользователей, а также предмета автопрограммы, его технических характеристик и стоимости.</p>
                <p>1.2. Компания обязуется предпринимать все необходимые организационные и технические меры по защите конфиденциальной информации от несанкционированного доступа со стороны третьих лиц. В процессе передачи данных и осуществления платежей используются зашифрованные каналы, безопасное соединение и новейшие разработки в сфере кибербезопасности.</p>
                <p>1.3. Со своей стороны участник автопрограммы обязуется предпринимать все доступные ему меры по защите личного аккаунта от стороннего вмешательства. Компания не является ответственным лицом при утечке информации, вызванной нарушением мер безопасности со стороны пользователя. Последствиями попадания данной информации к третьим лицам может являться взлом учётной записи, несанкционированный вывод средств, а также вынужденная блокировка аккаунта пользователя в связи с подозрительной или вредоносной активностью.</p>
                <p> <strong>2. Цели сбора и обработки полученных данных:</strong></p>
                <p>2.1. идентификация пользователя для безопасного и прозрачного сотрудничества;</p>
                <p>2.2. осуществление обратной связи с клиентами, включающей рассылку уведомлений и прочей информации относительно работы сервиса;</p>
                <p>2.3. оптимизация качества работы сайта и предоставления услуг, разработка новых сервисов для лучшего взаимодействия и т.д.;</p>
                <p>2.4. статистический анализ и учёт.</p>
                <p> <strong>3. Сбор данных</strong></p>
                <p>3.1. Собирается следующая информация:</p>
                <p>3.1.1 регистрационные данные (за исключением пароля от входа в личный кабинет);</p>
                <p>3.1.2 информация, передаваемая пользователем в процессе использования сайта, включая файлы cookie, IP-адрес, используемый браузер и пр.;</p>
                <p>3.1.3 платёжные реквизиты и другая информация, получаемая в процессе взаимодействия с клиентом.</p>
                <p>3.2. Личные данные пользователей хранятся на электронных носителях и обрабатываются автоматизированными системами, исключения составляют случаи, когда ручная обработка данных требуется в целях исполнения требований законодательства.</p>
                <p>3.3. Пользователь может запретить передачу cookie-файлов в настройках своего веб-обозревателя. Внимание: при отключении Javascript возможна некорректная работа браузера.</p>
                <p>3.4. Пользователь обязуется предоставлять исключительно достоверные персональные данные, а в случае потери их актуальности своевременно обновлять данную информацию. Пользователь имеет возможность в любой период времени изменять или дополнять эти сведения, используя функцию редактирования в личном кабинете. Пользователь имеет возможность удалить аккаунт вместе со своими регистрационными данными.</p>
                <p>3.5. Создание учётной записи на данном ресурсе означает согласие пользователя на хранение и обработку переданных данных, дополнительное подтверждение его согласия не требуется.</p>
                <p> <strong>4. Уничтожение данных</strong></p>
                <p>4.1. Личные данные пользователя уничтожаются в случае удаления аккаунта, по личному запросу пользователя либо по инициативе администрации ресурса в связи с нарушениями его владельцем установленных правил автопрограммы.</p>
                <p> <strong>5. Администрация компании оставляет за собой право вносить изменения и дополнения в политику конфиденциальности без предварительного или последующего уведомления пользователей.</strong></p>
                <p><strong>6. Участник имеет право распоряжаться фастерами на своё усмотрение, покупая или продавая их на внутренней P2P-бирже в любом необходимом объёме в соответствии с лимитами купленной лицензии.</strong></p>
            </div>
        </div>
    </section>
{{--    <article class="page">--}}
{{--        <section>--}}
{{--            <div class="container">--}}
{{--                <div class="page-top">--}}
{{--                    <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('Agreement')}}</span>--}}
{{--                    </div>--}}
{{--                    <h1 class="page-title">{{__('Agreement')}}--}}
{{--                    </h1>--}}
{{--                </div>--}}
{{--                <div class="typography">--}}
{{--                    <p>{!! __('This user agreement is required before you can start using the platform. Ignorance of the terms of cooperation cannot serve as an argument in favor of the client in case of disputes and conflicts.') !!}</p>--}}
{{--                    <h4>{!! __('1. General') !!}</h4>--}}
{{--                    <p>{!! __('1.1. Use of functions and capabilities of the resource is possible only after the user has passed registration according to the procedure established by the administration and authorization in the personal office.') !!}</p>--}}
{{--                    <p>{!! __("1.2. The user's registration on this resource is confirmation of his/her consent to non-disclosure of all confidential and protected data (including login and password for authorization), observance of the company's copyright to the posted materials and content of the website. The information in this document is not an investment cooperation proposal, particularly for those jurisdictions that consider non-public offers/petitions illegal.") !!}</p>--}}
{{--                    <p>{!! __('1.3. The customer agrees to use this site exclusively by legal means, without going beyond the legal norms of his jurisdiction.') !!}</p>--}}
{{--                    <p>{!! __('1.4. Authorized employees of the company can make changes and amendments to the rules and agreements, change the interest of the trade commission and commission on withdrawal of USD, as well as the terms of cooperation with clients without obligatory corresponding notification. The new version of the user agreement takes effect when it is posted in this section or after a special system alert for users. At the same time, familiarization with current rules of work and user agreement is the personal responsibility of the user.') !!}</p>--}}
{{--                    <h4>{!! __('2. Purchase ACC') !!}</h4>--}}
{{--                    <p>{!! __('2.1. The direct purpose of ACC accelerators is to activate the accelerated interest accrual mechanism in the WEC cryptocurrency stealing system.') !!} </p>--}}
{{--                    <p>{!! __('2.2. We do not guarantee the instant acquisition of ACC after the purchase order is issued.') !!}</p>--}}
{{--                    <p>{!! __('2.3. The company does not guarantee or facilitate the instant sale of the ACС, the speed of the transaction completion depends solely on the participants of the system.') !!}</p>--}}
{{--                    <p>{!! __('The internal exchange of purchases and sales of ACC is a platform for transactions between holders of AСС. We only provide a P2P-Exchanger for purchases and sales of AСС between users, without being responsible for the transactions under the ACC.') !!} </p>--}}
{{--                    <p>{!! __('2.4. The AСС value at the time of the purchase requisition creation and the value of ACC at the time of transaction conclusion may vary depending on the current market price of ACC.') !!}</p>--}}
{{--                    <p>{!! __('2.5. ACC is not a commercial entity and does not guarantee profit.') !!}</p>--}}
{{--                    <h4>{!! __('3. The cost of WEC licenses and coins') !!}</h4>--}}
{{--                    <p>{!! __('3.1. The Company reserves the right to change the value of licenses offered for purchase.') !!}</p>--}}
{{--                    <p>{!! __('3.2. The Company has the right to make changes to the terms of the referral program, including the amount of interest charges to the partners for the targeted actions of their referrals.') !!}</p>--}}
{{--                    <p>{!! __('3.3. The company does not guarantee the sale of WEC at a specific price. Since all WEC purchase and sale transactions are carried out on third-party cryptocurrency exchanges, the value of WEC coins in dollar (or in another fiat currency) equivalent is established on the basis of market demand and supply.') !!}</p>--}}
{{--                    <h4>{!! __('4. Field of responsibility') !!}</h4>--}}
{{--                    <p>{!! __('4.1. The Company shall not be liable for any actual or indirect losses of the system participant incurred as a result of:') !!}</p>--}}
{{--                    <ul>--}}
{{--                        <li>{!! __('personally made decisions on the use of some WEC cryptocurrency and ACC accelerators;') !!}</li>--}}
{{--                        <li>{!! __('technical problems on the part of the user (e.g. erroneous payment details) or incorrect operation of payment systems;') !!}</li>--}}
{{--                        <li>{!! __("blocking of the user's account due to financial or reputational damage to the company, violation of the rules of this user agreement, violation of the rules of the partner program or conducting other fraudulent activities on the company's website;") !!}</li>--}}
{{--                        <li>{!! __('force majeure.') !!}</li>--}}
{{--                    </ul>--}}

{{--                    <p>{!! __('4.2. The company is responsible for:') !!}</p>--}}
{{--                    <ul>--}}
{{--                        <li>{!! __("ensuring the site's health, operative resolution of any technical and software problems and errors;") !!}</li>--}}
{{--                        <li>{!! __('protection of financial resources and personal data of users from loss and unauthorized access by third parties;') !!}</li>--}}
{{--                        <li>{!! __('providing access to all sections of the site necessary for financial activities;') !!}</li>--}}
{{--                        <li>{!! __('compliance with financial obligations to clients and partners in accordance with the proposed terms of cooperation;') !!}</li>--}}
{{--                        <li>{!! __('providing clients with the opportunity to independently manage their funds by entering funds into the account balance and withdrawing funds to these electronic purses, as well as making reinvestments to increase income.') !!}</li>--}}
{{--                    </ul>--}}

{{--                    <h4>{!! __('5. Support') !!}</h4>--}}
{{--                    <p>{!! __('5.1. Technical Support is available to help you resolve issues and problems with the system members. All contact information is published in the appropriate section.') !!}</p>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
{{--    </article>--}}
@endsection