


<div class="pagination">
        @foreach ($elements as $element)

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)

                    @if ($paginator->currentPage() > 4 && $page === 2)
                        <span class="spread">...</span>
                    @endif

                    @if ($page == $paginator->currentPage())

                            <a class="is-active"  href="#">{{$page}}</a>

                        @elseif ($page === $paginator->currentPage() + 1 || $page === $paginator->currentPage() + 2 || $page === $paginator->currentPage() - 1 || $page === $paginator->currentPage() - 2 || $page === $paginator->lastPage() || $page === 1)

                            <a href="{{ $url }}">{{ $page }}</a>

                    @endif


                    @if ($paginator->currentPage() < $paginator->lastPage() - 3 && $page === $paginator->lastPage() - 1)
                            <a  href="#">...</a>
                    @endif
                @endforeach
            @endif
        @endforeach

    </div>




