<?php
namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectUser
 * @package App\Models
 *
 * @property string id
 * @property string project_id
 * @property string user_id
 * @property string login
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class ProjectUser extends Model
{
    use Uuids;
    use ModelTrait;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /** @var array $fillable */
    protected $fillable = [
        'project_id',
        'user_id',
        'login',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }


}
