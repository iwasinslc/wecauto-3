<?php
namespace App\Http\Controllers\Profile;

use App\Facades\WithdrawF;
use App\Http\Controllers\Controller;
use App\Http\Requests\RequestWithdraw;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;

/**
 * Class WithdrawController
 * @package App\Http\Controllers\Profile
 */
class WithdrawController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
//
//        if (!user()->hasRole(['root']))
//        {
//            return back()->with('error', __('Function dont working'));
//        }


        $pzm_ps = \App\Models\PaymentSystem::whereHas('currencies', function ($query) {
            $query->whereIn('code',['PZM']);
        })->get();

        $usd_ps = \App\Models\PaymentSystem::whereHas('currencies', function ($query) {
            $query->whereIn('code',['USD', 'BTC', 'ETH', 'USDT']);
        })->get();

        $wec_ps = \App\Models\PaymentSystem::whereHas('currencies', function ($query) {
            $query->whereIn('code',['WEC']);
        })->get();

        return view('profile.withdraw', [
            'usd_ps'=>$usd_ps,
            'wec_ps'=>$wec_ps,
            'pzm_ps'=>$pzm_ps,

        ]);
    }

    /**
     * @param RequestWithdraw $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(RequestWithdraw $request)
    {
        $user = user();

        $extractCurrency = explode(':', $request->currency);

        if (count($extractCurrency) != 2) {
            return redirect()->route('profile.withdraw')->with('error', __('Unable to read data from request'))->withInput();
        }

        $paymentSystem = PaymentSystem::where('id', $extractCurrency[1])->first();
        $currency = $paymentSystem->currencies()->where('id', $extractCurrency[0])->first();

        /**
         * @var Wallet $wallet
         */
        $wallet = $user->wallets()->where('currency_id', $currency->id)->where('payment_system_id', $paymentSystem->id)->first();

        //$commission = (float)Setting::getValue('commission_'.strtolower($wallet->currency->code));

        $amount = $request->amount;

        if ($currency->code=='ACC')
        {
            $limit = $user->sellLimit()*rate('USD', 'ACC');

            if ($limit<$request->amount)
            {
                return redirect()->route('profile.withdraw')->with('error',__('Limits is done'));
            }
        }

        if (!in_array($currency->code, ['WEC', 'BIP', 'PZM', 'ACC']))
        {
            //$commission = $request->amount*(float)Setting::getValue('commission_'.strtolower($wallet->currency->code))*0.01;

            $wallet = user()->wallets()->where('payment_system_id', \App\Models\PaymentSystem::getByCode('perfectmoney')->id)
                ->where('currency_id', Currency::getByCode('USD')->id)
                ->first();
        }

        $external = str_replace(['pb:', 'wt:'], ['PB:', 'WT:'],$request->external);

        if ($request->has('comment')&&!empty($request->comment)&&$currency->code=='PZM')
        {
            $external = $request->external.':'.$request->comment;
        }

        $with_confirmation = false;
        try {
            WithdrawF::create($user, $wallet, $amount, $paymentSystem, $currency, $external, $with_confirmation);
            if ($currency->code=='ACC')
            {
                $user->removeLimitsSell($amount, $currency->code);
            }
            if($with_confirmation) {
                return back()->with('success', __('Confirm withdraw by link in the email. Than it will be sent to administrator.'));
            } else {
                return back()->with('success', __('Request has been sent to administration'));
            }
        } catch(\Exception $e) {
            return redirect()->route('profile.withdraw')->with('error', $e->getMessage());
        }
    }


    public function reject($transaction)
    {
        try {
            WithdrawF::reject($transaction, user());
            $status = 'success';
            $message = __('Request rejected');
        } catch (\Throwable $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        return back()->with($status, $message);
    }
}
