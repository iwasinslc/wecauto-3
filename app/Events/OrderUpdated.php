<?php

namespace App\Events;

use App\Models\Notification;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $broadcastQueue = 'default';

    /** @var array $balanceData */
    public $data;

    /**
     * MyBalanceUpdated constructor.
     * @param Wallet $wallet
     */
    public function __construct(Transaction $transaction)
    {


        \Log::debug(self::class.' event: '.print_r($transaction,true));


        /** @var array balanceData */
        $this->data = [
            'user_id'=>$transaction->user_id,
            'amount'=>$transaction->amount,
            'currency'=>$transaction->currency->code,
            'sell_limit'=>$transaction->user->sellLimit(),
            'buy_limit'=>$transaction->user->buyLimit()
        ];


    }

    /**
     * @return array|Channel|Channel[]
     */
//    public function broadcastOn()
//    {
//        $ownerId = $this->balanceData['owner_id'];
//        return ['App.User.'.md5($ownerId.env('APP_KEY'))];
//    }


    public function broadcastOn()
    {
        $ownerId = $this->data['user_id'];
        return new PrivateChannel('user.'.$ownerId);
    }

}
