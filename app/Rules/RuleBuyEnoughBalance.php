<?php
namespace App\Rules;

use App\Models\Setting;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RuleEnoughBalance
 * @package App\Rules
 */
class RuleBuyEnoughBalance implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $wallet = user()->wallets()->where('id', request('wallet_id'))->first();

        if (empty($wallet)) {
            return false;
        }

        $amount = request()->get('amount');

        $rate = Setting::getValue('wec_price');
        $amount = $amount*$rate;
        return $wallet ? $wallet->balance >= $amount : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.enough_balance');
    }
}
